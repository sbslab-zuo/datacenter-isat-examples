# README #

This is a data center case using ISAT module in Modelica Buildings library. This work is from outcomes of a DoE project "Improving Data Center Energy Efficiency through End-to-End Cooling Modeling and Optimization" (DE-EE0007688).

### What is this repository for? ###

* ISAT module is a detailed room model in Modelica Buildings library.
* This repository stores a data center case using the ISAT module.

### How do I get set up? ###

* The model in this repository is dependent on Modelica(version="3.2.2"), Buildings(version="7.0.0") libraries.
* Refer to Buildings.ThermalZones.Detailed.UsersGuide.ISAT in Modelica Buildings library: https://github.com/lbl-srg/modelica-buildings

### Contribution guidelines ###

* You are welcomed to contribute this repository.

### Who do I talk to? ###

* If you have any questions, please contact: xuha3556@colorado.edu
* Refer to the project website for more information: https://www.colorado.edu/lab/sbs/doe-datacenter