within ;
package ISAT "Repository that stores a data center example for Buildings.ThermalZones.Detailed.ISAT model"
annotation (uses(Modelica(version="3.2.2"), Buildings(version="7.0.0")),
  version="2",
  conversion(noneFromVersion="", noneFromVersion="1"));
end ISAT;
