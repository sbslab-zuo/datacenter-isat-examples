within ISAT;
package Examples "Package for a data center case"
  extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>
This package stores a data center case
</p>
</html>", revisions="<html>
<ul>
<li>April 5, 2020, by Xu Han, Cary Faulkner, Wangda Zuo:<br>First implementation. </li>
</ul>
</html>"));
end Examples;
